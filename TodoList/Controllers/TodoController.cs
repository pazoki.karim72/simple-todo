using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using TodoList.Models;

namespace TodoList.Controllers;

[ApiController]
[Route("[controller]")]
public class TodoController : ControllerBase
{
    private readonly ILogger<TodoController> _logger;
    private TodoDbContext _dbContext;

    public TodoController(ILogger<TodoController> logger, TodoDbContext dbContext)
    {
        _logger = logger;
        _dbContext = dbContext;
    }

    [HttpGet(Name = "GetTodo")]
    public IEnumerable<Todo> Get()
    {
        return _dbContext.Todos.ToList();
    }
    
    [HttpPost(Name = "AddTodo")]
    public void Add(Todo todoItem)
    {
        var task = new Todo()
        {
            title = todoItem.title,
            status = todoItem.status
        };

        _dbContext.Todos.Add(task);
        _dbContext.SaveChanges();
    }
    
    [HttpPut("{id}", Name = "updateTodo")]
    public void changeStatus(int id, Todo todoItem)
    {
        var task = _dbContext.Todos.Find(id);
        if (task != null)
        {
            task.title = todoItem.title;
            task.status = todoItem.status;
            _dbContext.SaveChanges();
        }
    }
    
    [HttpDelete("{id}", Name = "deleteTodo")]
    public void delete(int id)
    {
        var task = _dbContext.Todos.Find(id);
        if (task != null)
        {
            _dbContext.Todos.Remove(task);
            _dbContext.SaveChanges();
        }
    }
}